<?php

/**
 * Contrôleur de gestion des representation
 * @author prof
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\RepresentationDAO;
use modele\dao\AttributionDAO;
use modele\metier\Representation;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;
use modele\metier\Groupe;
use modele\metier\Lieu;
use vue\representations\VueConsultationRepresentations;
use vue\representations\VueSaisieRepresentations;
use vue\representations\VueSupprimerRepresentations;

class CtrlRepresentations extends ControleurGenerique {

    /** controleur= representation & action= defaut
     * Afficher la liste des representation      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= representation & action= liste
     * Afficher la liste des representation      */
    public function liste() {
        $laVue = new VueConsultationRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representation avec, pour chacun,
        //  son nombre d'attributions de chambres actuel : 
        //  on ne peut supprimer un representation que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        //$laVue->setLesGroupes();
        //$laVue->setLesLieux(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'un representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle representation");
        $leGroupe = new Groupe("", "", "", "", "", "", "");
        $leLieu = new Lieu("", "", "", "");
        // En création, on affiche un formulaire vide
        /* @var representation $unEtab */
        $unEtab = new Representation("", "", "", "", $leLieu, $leGroupe);
        Bdd::connecter();
        $laVue->setLesLieux(LieuDAO::getAll());
        $laVue->setLesGroupes(GroupeDAO::getAll());
        $laVue->setUneRepresentation($unEtab);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerCreer
     * ajouter d'un representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        $lieu = null;
        $groupe = null;
        $id = $_REQUEST['id'];
        $daterepresentation = $_REQUEST['dateRepresentation'];
        $heuredebut = $_REQUEST['heureDebut'];
        $heurefin = $_REQUEST['heureFin'];
        $idlieu = $_REQUEST['idLieu'];
        $idgroupe = $_REQUEST['idGroupe'];

        if ($idlieu == "" || $idgroupe == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
            header("Location: index.php?controleur=representations&action=creer");
        } else {
            $lieu = LieuDAO::getOneById($idlieu);
            $groupe = GroupeDAO::getOneById($idgroupe);

            /* @var representation $unEtab  : récupération du contenu du formulaire et instanciation d'un representation */
            $uneRepre = new Representation($id, $daterepresentation, $heuredebut, $heurefin, $lieu, $groupe);


            // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
            // pour un formulaire de création (paramètre n°1 = true)
            $this->verifierDonneesRep($uneRepre, true);
            if (GestionErreurs::nbErreurs() == 0) {
                // s'il ny a pas d'erreurs,
                // enregistrer l'representation
                RepresentationDAO::insert($uneRepre);
                // revenir à la liste des representation
                header("Location: index.php?controleur=representations&action=liste");
            } else {
                // s'il y a des erreurs, 
                // réafficher le formulaire de création
                $laVue = new VueSaisieRepresentations();
                $this->vue = $laVue;
                $laVue->setActionRecue("creer");
                $laVue->setActionAEnvoyer("validerCreer");
                $laVue->setMessage("Nouvel representation");
                $leGroupe = new Groupe("", "", "", "", "", "", "");
                $leLieu = new Lieu("", "", "", "");
                // En création, on affiche un formulaire vide
                /* @var Representation $uneRep */
                $uneRep = new Representation("", "", "", "", $leLieu, $leGroupe);
                Bdd::connecter();
                $laVue->setLesLieux(LieuDAO::getAll());
                $laVue->setLesGroupes(GroupeDAO::getAll());

                $laVue->setUneRepresentation($uneRepre);
                parent::controlerVueAutorisee();
                $laVue->setTitre("Festival - representaion");
                $laVue->setVersion($this->version);
                $this->vue->afficher();
            }
        }
    }

    /** controleur= representation & action=modifier $ id=identifiant de l'representation à modifier
     * Afficher le formulaire de modification d'un representation     */
    public function modifier() {
        $idRep = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'representation à modifier
        Bdd::connecter();
        $laVue->setLesLieux(LieuDAO::getAll());
        $laVue->setLesGroupes(GroupeDAO::getAll());
        /* @var representation $lerepresentation */
        $laRepresentation = RepresentationDAO::getOneById($idRep);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laRepresentation->getId() . " (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier un representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        $lesLieux = LieuDAO::getAll();
        $lesGroupes = GroupeDAO::getAll();
        /* @var representation $unEtab  : récupération du contenu du formulaire et instanciation d'un representation */
        $unLieu = LieuDAO::getOneById($_REQUEST['idLieu']);
        $unGroupe = GroupeDAO::getOneById($_REQUEST['idGroupe']);
        $uneRep = new Representation($_REQUEST['id'], $_REQUEST['dateRepresentation'], $_REQUEST['heureDebut'], $_REQUEST['heureFin'], $unLieu, $unGroupe);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRep($uneRep, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'representation
            RepresentationDAO::update($uneRep->getId(), $uneRep);
            // revenir à la liste des representation
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setLesLieux($lesLieux);
            $laVue->setLesGroupes($lesGroupes);
            $laVue->setUneRepresentation($uneRep);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier l'représentation : " . $uneRep->getLieu()->getNom() . " (" . $uneRep->getGroupe()->getNom() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitresetUneRepresentation("Festival - representations");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }

    /** controleur= representation & action=supprimer & id=identifiant_representation
     * Supprimer un representation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentations();
        // Lire dans la BDD les données de l'representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= representation & action= validerSupprimer
     * supprimer un representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de l'representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representation
        header("Location: index.php?controleur=representations&action=liste");
    }

    /*     * ***************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     * **************************************************************************** */

    /**
     * Retourne la liste de tous les representations et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id de l'representation
     *      - dimension 2, index "etab" => objet de type representation
     *      - dimension 2, index "nbAttrib" => nombre d'attributions pour cet representation
     */
    public function getTabRepresentationsGroupes(): Array {
        $lesRepresentationsGroupes = Array();
        $lesRepresentations = RepresentationDAO::getAll();
        foreach ($lesRepresentations as $uneRepre) {
            /* @var representation $unEtab */
            $lesRepresentationsGroupes[$uneRepre->getId()] = GroupeDAO::getOneById($uneRepre);
        }
        return $lesRepresentationsAvecNbAttrib;
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Representation $uneRep representation à vérifier
     * @param bool $creation : =true si formulaire de création d'une representation  ; =false sinon
     */
    private function verifierDonneesRep(Representation $uneRep, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRep->getId() == "") || $uneRep->getDateRepresentation() == "" ||
                $uneRep->getHeureDebut() == "" || $uneRep->getHeureFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRep->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estEntier($uneRep->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRep->getId())) {
                    GestionErreurs::ajouter("Le representation " . $uneRep->getId() . " existe déjà");
                }
            }
        }
    }

}
