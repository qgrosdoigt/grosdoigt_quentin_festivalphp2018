<?php

/**
 * Contrôleur de gestion des établissements
 * @author prof
 * @version 2018
 */

namespace controleur;
use controleur\GestionErreurs;
use modele\dao\AttributionDAO;
use modele\dao\GroupeDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupe\VueListeGroupes;
use vue\groupe\VueSaisieGroupe;
use vue\groupe\VueSupprimerGroupe;

class CtrlGroupes extends ControleurGenerique {

    /** controleur= etablissements & action= defaut
     * Afficher la liste des établissements      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= etablissements & action= liste
     * Afficher la liste des établissements      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des établissements avec, pour chacun,
        //  son nombre d'attributions de chambres actuel : 
        //  on ne peut supprimer un établissement que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesGroupes($this->getTabGroupes());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }
    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau groupe");
        // En création, on affiche un formulaire vide
        /* @var GRoupe $unEtab */
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $laVue->setUnGroupe($unGroupe);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupe");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= groupe & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identite'], $_REQUEST['adresse'], $_REQUEST['nbPers'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvel groupe");
            $laVue->setUnGroupe($unGroupe);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupe");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }

    /** controleur= groupe & action=modifier $ id=identifiant du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGroupe = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'établissement à modifier
        Bdd::connecter();
        /* @var Etablissement $leEtablissement */
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        $this->vue->setUnGroupe($leGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupe->getNom() . " (" . $leGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupe");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= groupe & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identite'], $_REQUEST['adresse'], $_REQUEST['nbPers'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);

        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour le groupe
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGroupe->getNom() . " (" . $unGroupe->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupe");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }
    /** controleur= etablissements & action=supprimer & id=identifiant_établissement
     * Supprimer un établissement d'après son identifiant     */
    public function supprimer() {
        $idGroup = $_GET["id"];
        $this->vue = new VueSupprimerGroupe();
        // Lire dans la BDD les données de l'établissement à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroup));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupe");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= etablissements & action= validerSupprimer
     * supprimer un établissement dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de l'établissement à supprimer");
        } else {
            // suppression de l'établissement d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=groupes&action=liste");
    }
    
        /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    /**
     * Retourne la liste de tous les Groupes et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id de l'établissement
     *      - dimension 2, index "etab" => objet de type Groupe
     *      - dimension 2, index "nbAttrib" => nombre d'attributions pour cet établissement
     */
    public function getTabGroupes(): Array {
        $lesGroupes= Array();
        $lesGroupes2 = GroupeDAO::getAll();
        foreach ($lesGroupes2 as $unGroupe) {
            /* @var Groupe $unGroupe */
            $lesGroupes[$unGroupe->getId()]['groupe'] = $unGroupe;
        }
        return $lesGroupes;
    }

      private function verifierDonneesGroupe(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
    if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || $unGroupe->getIdentite() == "" || $unGroupe->getAdresse() == "" ||
                $unGroupe->getNbPers() == "" || $unGroupe->getNomPays() == "" || $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("Le groupe " . $unGroupe->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'un établissement de même nom n'existe pas déjà (id + nom si création)
            if ($unGroupe->getNom() != "" && GroupeDAO::isAnExistingName($creation, $unGroupe->getId(), $unGroupe->getNom())) {
            GestionErreurs::ajouter("Le groupe " . $unGroupe->getNom() . " existe déjà");
        }
        
        if ($unGroupe->getNom() != ""){
            // Si l'id est constitué que de chiffres, une erreur sera généré.
            if (!estLettre($unGroupe->getNom())){
                ajouterErreur("Le groupe ne doit contenir que des lettres");
        }
    }
    }


}
