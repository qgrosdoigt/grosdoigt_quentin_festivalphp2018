<?php
namespace modele\metier;

/**
 * Description d'un représentation
 * 
 * @author eleve jsamson
 */
class Representation {
    /** identifiant de la representation
     * @var int
     */
    private $id;
    /**
     * date de la représentation
     * @var string
     */
    private $dateRepresentation;
    /**
     * heure de début de la représentation
     * @var string
     */
    private $heureDebut;
    /**
     * heure de fin de la représentation
     * @var string
     */
    private $heureFin;
    /**
     * identifiant du lieu de representation
     * @var modele\metier\Lieu
     */
    private $lieu;
    /**
     * identifiant du groupe faisant la representation
     * @var modele\metier\Groupe
     */
    private $groupe;
    
    
    function __construct($id, $dateRepresentation, $heureDebut, $heureFin, Lieu $lieu, Groupe $groupe) {
        $this->id = $id;
        $this->dateRepresentation = $dateRepresentation;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
        $this->lieu = $lieu;
        $this->groupe = $groupe;
    }
    function getId() {
        return $this->id;
    }

    function getDateRepresentation() {
        return $this->dateRepresentation;
    }

    function getHeureDebut() {
        return $this->heureDebut;
    }

    function getHeureFin() {
        return $this->heureFin;
    }

    function getLieu(): Lieu {
        return $this->lieu;
    }

    function getGroupe(): Groupe {
        return $this->groupe;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDateRepresentation($dateRepresentation) {
        $this->dateRepresentation = $dateRepresentation;
    }

    function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }

    function setLieu(Lieu $lieu) {
        $this->lieu = $lieu;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }



}
