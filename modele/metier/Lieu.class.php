<?php
namespace modele\metier;

/**
 * Description d'un lieu de représentation
 * 
 * @author eleve jsamson
 */
class Lieu {
    /** identifiant du lieu
     * @var int
     */
    private $id;
    /** nom du lieu
     * @var string
     */
    private $nom;
    /** adresse du lieu
     * @var string
     */
    private $adresse;
    /** capacité d'accueil du lieu
     * @var int
     */
    private $capaciteAccueil;
    
    function __construct($id, $nom, $adresse, $capaciteAccueil) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capaciteAccueil = $capaciteAccueil;
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCapaciteAccueil() {
        return $this->capaciteAccueil;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCapaciteAccueil($capaciteAccueil) {
        $this->capaciteAccueil = $capaciteAccueil;
    }



}
