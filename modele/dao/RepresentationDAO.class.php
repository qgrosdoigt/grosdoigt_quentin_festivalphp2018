<?php
namespace modele\dao;

use modele\metier\Representation;
use PDOStatement;
use PDO;


/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author eleve
 * @version 2017
 */
class RepresentationDAO {


    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table Representation
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $dateRepresentation = $enreg['DATEREPRESENTATION'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        $lieu = $enreg['IDLIEU'];
        $groupe = $enreg['IDGROUPE'];
        $objetLieu = LieuDAO::getOneById($lieu);
        $objetGroupe = GroupeDAO::getOneById($groupe);
        $uneRepresentation = new Representation($id, $dateRepresentation, $heureDebut, $heureFin, $objetLieu, $objetGroupe);

        return $uneRepresentation;
    }
    
    
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param Representation $objetMetier un lieu
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':dateRepresentation', $objetMetier->getDateRepresentation());
        $stmt->bindValue(':heureDebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heureFin', $objetMetier->getHeureFin());
        $stmt->bindValue(':idLieu', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':idGroupe', $objetMetier->getGroupe()->getId());
    }


    /**
     * Retourne la liste de toutes les representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY DATEREPRESENTATION";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

        /**
     * Liste des objets Attribution concernant un établissement donné
     * @param string $idEtab : identifiant de l'établissement dont on filtre les attributions
     * @return array : tableau d'Attribution(s)
     */
    public static function getAllByDate($uneDate) {
        $lesRepresentationsParDate = array();
        $requete = "SELECT * FROM Representation WHERE DATEREPRESENTATION = dateRepresentation ORDER BY dateRepresentation,heureDebut";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':dateRep', $uneDate);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesRepresentationsParDate[] = self::enregVersMetier($enreg);
            }
        }
        return $lesRepresentationsParDate;
    }
    
    
    
    
    
       /**
     * Retourne la liste de toutess les representations SELECT date_rep,l.nom,g.nom,heureDebut,heureFin FROM Representation r INNER JOIN Groupe g ON r.id_groupe = g.id INNER JOIN Lieu l ON r.id_lieu = l.id
     * @return array tableau d'objets de type Representation
     */
    public static function getAllDate() {
        $lesDates = array();
        $requete = "SELECT Distinct daterepresentation FROM Representation ORDER BY daterepresentation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesDates[] = $enreg['DATEREPRESENTATION'];
            }
        }
        return $lesDates;
    }
    
        public static function getAllbyLieu() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
        public static function getAllbyGroupe() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY id_groupe";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    /**
     * Recherche une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }


    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet representation métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation (id,daterepresentation,idlieu,idgroupe,heureDebut,heureFin) VALUES (:id, :dateRepresentation, :idLieu, :idGroupe, :heureDebut, :heureFin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    
    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET DATEREPRESENTATION=:dateRepresentation, HEUREDEBUT=:heureDebut, HEUREFIN=:heureFin, IDLIEU=:idLieu, IDGROUPE=:idGroupe"
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Détruire un enregistrement de la table Representation d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non un lieu ayant déjà le même identifiant dans la BD
     * @param string $id identifiant du lieu à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

    
}
