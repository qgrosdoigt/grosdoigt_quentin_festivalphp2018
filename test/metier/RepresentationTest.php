<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
//        $dateRep = new date("Y-m-d", '2018-11-20');
//        $heureDeb = new date("h:i:sa", '10:34:09');
//        $heureFin = new date("h:i:sa", '11:34:09');
        $unGroupe = new Groupe("g999","les Joyeux Turlurons","général Alcazar","Tapiocapolis" ,25,"San Theodoros","N");
        $unLieu = new Lieu(1, 'NomLieu', 'AdresseLieu', 20);
        $uneRepresentation = new Representation(1,'2018-11-20', '10:34:09', '11:34:09', $unLieu, $unGroupe);
        var_dump($uneRepresentation);
        ?>
    </body>
</html>
