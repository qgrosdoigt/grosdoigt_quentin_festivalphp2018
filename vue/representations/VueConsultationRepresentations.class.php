<?php
/**
 * Description Page de consultation des offres d'hébergement par établissmeent
 * -> affiche une page comportant un tableau par établissement, indiquant 
 * pour chaque type de chambre, le nombre de chambres offertes pour cet établissment
 * @author prof
 * @version 2018
 */

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

class VueConsultationRepresentations extends VueGenerique {

    /** @var array liste des dates de représentations */
    private $lesRepresentations;
    
    /** @var Array liste des lieu des représentations */
    private $lesLieux;
    
    /** @var Array liste des groupes des représentations */
    private $lesGroupes;

    private $dates;
    private $vari;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
            $dates='2016-08-20';
            $vari=1;
        include $this->getEntete();
        // IL FAUT QU'IL Y AIT AU MOINS UN ÉTABLISSEMENT ET UN TYPE CHAMBRE POUR QUE L'AFFICHAGE SOIT EFFECTUÉ        
        if (count($this->lesRepresentations) != 0) {
            // POUR CHAQUE ÉTABLISSEMENT : AFFICHAGE DU NOM ET D'UN TABLEAU COMPORTANT 1
            // LIGNE D'EN-TÊTE ET 1 LIGNE PAR TYPE DE CHAMBRE
            foreach ($this->lesRepresentations as $uneRepresentation) {
                // AFFICHAGE DU NOM DE L'ÉTABLISSEMENT ET D'UN LIEN VERS LE FORMULAIRE DE MODIFICATION
                   
                if($dates!=$uneRepresentation->getDateRepresentation())
                {
                ?>
                 <?php if($vari!=1){
                     echo'</table><br>';}?>
                <strong><?= $uneRepresentation->getDateRepresentation() ?></strong><br>
                
                
                <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                    <tr class="enTeteTabQuad">
                        <td width="30%">Lieu</td>
                        <td width="35%">Groupe</td>
                        <td width="35%">Heure de début</td> 
                        <td width="35%">Heure de fin</td> 
                        <td></td>
                        <td></td>
                    </tr>
                <?php } ?>
                        <tr class="ligneTabQuad">
                            <td><?= $uneRepresentation->getLieu()->getNom() ?></td>
                            <td><?= $uneRepresentation->getGroupe()->getNom() ?></td>
                            <td><?= $uneRepresentation->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation->getHeureFin() ?></td>
                            <td><a href="index.php?controleur=representations&action=modifier&id=<?= $uneRepresentation->getId() ?>">Modifier</a></td>
                            <td><a href="index.php?controleur=representations&action=supprimer&id=<?= $uneRepresentation->getId() ?>">Supprimer</a></td>
                        </tr>
                        
               
                <?php
                $vari=2;
                $dates=$uneRepresentation->getDateRepresentation();
            }
            include $this->getPied();
        }
        ?>
        <br/>
        <a href="index.php?controleur=representations&action=creer" >Création d'une représentation</a >
        <?php
    }

    function setLesRepresentations(Array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }

    function setLesLieux(Array $lesLieux) {
        $this->lesLieux = $lesLieux;
    }

    function setLesGroupes(Array $lesGroupes) {
        $this->lesGroupes = $lesGroupes;
    }

}