<?php

// renvoie l'ensemble des groupes sous forme de flux json
//header("Content-type: text/xml");

/**
 * Contrôleur : gestion des offres d'hébergement
 */
use modele\dao\RepresentationDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;

//require_once __DIR__ . '/../includes/autoload.php';
require_once __DIR__ . '/../includes/autoload.inc.php';
//include("../includes/_gestionErreurs.inc.php");
Bdd::connecter();

$lesRepresentations = RepresentationDAO::getAll();
$nbLesRepresentations = count($lesRepresentations);

//encoding="iso-8859-1"
if ($nbLesRepresentations != 0) {
    echo json_encode($lesRepresentations);
}
?>

