<?php

// renvoie l'ensemble des groupes sous forme de flux XML
//header("Content-type: text/xml");

/**
 * getAllGroupesXML.php
 */
use modele\dao\RepresentationDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;
use controleur\Session;

//require_once __DIR__ . '/../includes/autoload.php';
require_once __DIR__ . '/../includes/autoload.inc.php';
//include("../iincludes/autoload.inc.php");
//require_once __DIR__ .'../includes/_gestionErreurs.inc.php';
Session::demarrer();
Bdd::connecter();


// utilisation de la couche DAO pour récupérer les groupes dans la BDD
$lesRepresentations = RepresentationDAO::getAll();

// génération du XML
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
echo "<lesrepresentations>\n";

// BOUCLE SUR LES REPRESENTIONS 
foreach ($lesRepresentations as $uneRepresentation) {
    echo "\t<representation>\n";

    echo "\t\t<id>" . $uneRepresentation->getId() . "</id>\n";
    echo "\t\t<date_representation>" . $uneRepresentation->getDateRepresentation() . "</date_representation>\n";
    echo "\t\t<heure_debut>" . $uneRepresentation->getHeureDebut() . "</heure_debut>\n";
    echo "\t\t<heure_fin>" . $uneRepresentation->getHeureFin() . "</heure_fin>\n";
    echo "\t\t<lieu>" . $uneRepresentation->getLieu()->getNom() . "</lieu>\n";
    echo "\t\t<numgroupe>" . $uneRepresentation->getGroupe()->getNom() . "</numgroupe>\n";

    echo "\t</representation>\n";
}
echo "</lesrepresentations>";
Bdd::deconnecter();
Session::arreter();
?>

